import settings
import sys
import os
from PIL import Image

class Picture:

    def __init__(self,inputPath,outputPath=None):
        self.inputPath = inputPath
        splittedInputPath = inputPath.split("/")
        self.path = '/'.join(splittedInputPath[:-1])
        self.fileName = splittedInputPath[-1]
        self.thumbnailName = "thumbnail_"+self.fileName

        if outputPath is None:
            self.outputPath = self.path

        self.outputPath+="/"+self.thumbnailName

    def generateThumbnail(self):
        size = settings.thumbnailSize


        try:
            im = Image.open(self.inputPath)
            im.thumbnail(size)
            im.save(self.outputPath, "JPEG")
        except IOError:
            print
            "cannot create thumbnail for", self.path

    def getPath (self):
        return self.path

